# Back-end Project
## Prerequisites
Make sure you have installed all of the following prerequisites on your development machine:

* Git - [Download & Install Git](https://git-scm.com/downloads).

* Node.js - [Download & Install Node.js](https://nodejs.org/en/download/) 

### Cloning The GitHub Repository

```bash
$ git clone https://digoribeiro@bitbucket.org/digoribeiro/accedo-api.git
```

To install the dependencies, run this in the application folder from the command-line:

```bash
$ cd accedo-api && npm install
```

## Running api

Run api using npm:

```bash
$ npm start
```

Your application should run on port 5000 [http://localhost:5000](http://localhost:5000)

## Demos

**API:** [http://accedo-teste-api-tv.umbler.net/videos](http://accedo-teste-api-tv.umbler.net/videos)

