module.exports = app => {
  var videoList = require('../controller/videosController');

  app.route('/videos').get(videoList.list_all_videos);
};
